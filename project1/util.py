#!/usr/bin/env python
import pymongo
import pprint
import pandas as pd
import datetime, xlrd
import json 
from xlrd import open_workbook
from pymongo import MongoClient
def getMongostatus():
    client = MongoClient('mongodb://localhost:27017/')
    print client
    return client

def getExceldata():
    data = pd.read_excel("test.xlsx")
    return data

def insertData(client):
    db = client.collegedb
    post = {"student_id" : "664", 
	        "basic_info":{
			"first_name" : "prasanna", 
            "last_name"  : "perumal", 
            "father_name":"viswalingam", 
            "department":"EEE", 
            "year":"FIRST", 
            "dob":"22.12.97", 
            "age":"18",
            "address":{
                "door_no":"30",
                "streetname":"main road", 
                "city":"chennai", 
                "state":"tamilnadu"}, 
	        "pincode":"612502", 
            "contact_no":"997679675"}, 
            "educational_details":{
			   "email_id":"prasanna@gmail.com", 
			   "name_of_course":"HSC", 
			   "name_of_institute":"s.s.n.hr.sec.school", 
			   "year_of_passing":"2015", 
			   "percentage":"80"}}
    studentdetails = db.studentdetails
    student_id =studentdetails.insert_one(post).inserted_id
    if db.studentdetails.find({'UserIDS': { "$in": student_id}}):
        print "inserted"   
        print student_id  
    else:
        print "not inserted"
    for post in studentdetails.find():
        pprint.pprint(post)

			
def printExcel(data):
      print data


def construct_json(row_index,col_range,sheet, book):
    stud_id_value= int(sheet.cell(row_index,0).value)
    fn_value = sheet.cell(row_index,1).value
    ln_value = sheet.cell(row_index,2).value
    father_name = sheet.cell(row_index,3).value
    dept_value = sheet.cell(row_index,4).value
    year_value = sheet.cell(row_index,5).value
    dob_value = sheet.cell(row_index,6).value
    age_value = int(sheet.cell(row_index,7).value)
    doorno_value = int(sheet.cell(row_index,8).value)
    streetname_value = sheet.cell(row_index,9).value
    city_value = sheet.cell(row_index,10).value
    state_value = sheet.cell(row_index,11).value
    pincode_value = int(sheet.cell(row_index,12).value)
    contact_value = int(sheet.cell(row_index,13).value)
    email_id_value = sheet.cell(row_index,14).value
    namecourse_value = sheet.cell(row_index,15).value
    name_of_institute_value = sheet.cell(row_index,16).value
    year_of_passing_value = int(sheet.cell(row_index,17).value)
    percentage_value = sheet.cell(row_index,18).value
    studentdetails ={"student_id":stud_id_value,
	                    "basic_info":{
				    	    "firstname":fn_value,
	       				    "lastname":ln_value, 
				            "father_name":father_name, 
				            "department":dept_value,
				            "year":year_value, 
			                 "dob":datetime.datetime(*xlrd.xldate_as_tuple(dob_value, book.datemode)), 
			               	 "age":age_value,
				         "address":{ 
					        "door_no":doorno_value,
					        "streetname":streetname_value, 
					        "city":city_value, 
					        "state":state_value,
					        "pincode":pincode_value},
					   "contact_no":contact_value, 
					   "email_id":email_id_value},
                       "educational_details":{
					      "name_of_course":namecourse_value,
					      "name_of_institute":name_of_institute_value,
					      "year_of_passing":year_of_passing_value,
					      "percentage":percentage_value},
			          "created_at":datetime.datetime.utcnow(),
			          "created_by":"surya",
			          "last_updated_at":datetime.datetime.utcnow(),
			          "last_updated_by": "surya"} 
    return studentdetails
	
def openExcel():
    book = open_workbook('test.xlsx')
    sheet = book.sheet_by_index(0)
    sheet1= book.sheet_by_index(1)
    return (sheet, sheet1, book)
    
def insert_bulk(client,dict_list):
    db = client.collegedb
    db.studentdetails.insert_many(dict_list).inserted_ids
    print("successfully inserted data rows. count=%s" % db.studentdetails.count())
	
def faculties_json(row_index, col_range, sheet1, book):
     facul_id_value= int(sheet1.cell(row_index,0).value)
     facul_name_value= sheet1.cell(row_index,1).value
     designation_value= sheet1.cell(row_index,2).value
     depart_value= sheet1.cell(row_index,3).value
     edu_quali_value= sheet1.cell(row_index,4).value
     doj_value= sheet1.cell(row_index,5).value
     experience_value=int(sheet1.cell(row_index,6).value)
     email_value= sheet1.cell(row_index,7).value
     contact_value= int(sheet1.cell(row_index,8).value)
     coursename_value= sheet1.cell(row_index,9).value
     institutename_value= sheet1.cell(row_index,10).value
     specializaion_value= sheet1.cell(row_index,11).value
     earofpassing_value= int(sheet1.cell(row_index,12).value)
     acc_no_value=int(sheet1.cell(row_index,13).value)
     acc_name_value= sheet1.cell(row_index,14).value
     bank_name_value= sheet1.cell(row_index,15).value
     ifsc_value=sheet1.cell(row_index,17).value
     branch_name_value= sheet1.cell(row_index,16).value
     pan_no_value=sheet1.cell(row_index,18).value
     facultydetails ={"faculty_id":facul_id_value,
                       "basic_info":{
				    	    "name":facul_name_value,
	       				    "designation":designation_value, 
				            "department":depart_value, 
				            "educational_qualification":edu_quali_value,
				            "doj":datetime.datetime(*xlrd.xldate_as_tuple(doj_value, book.datemode)), 
                            "experience":experience_value,		               
                            "email_id":email_value,
                            "contact_no":contact_value},
                       "educational_details":{
					      "name_of_course":coursename_value,
					      "name_of_institute":institutename_value,
					      "branch_specialization":specializaion_value,
                          "year_of_passing":earofpassing_value},
                       "bank_details":{
					       "account_number":acc_no_value,
                           "account_name":acc_name_value,
                           "bank_name":bank_name_value,
                           "branch":branch_name_value,
                           "ifsc_code":ifsc_value,
                           "pan_number":pan_no_value},			 
                       "created_at":datetime.datetime.utcnow(),
                       "created_by":"surya",
                       "last_updated_at":datetime.datetime.utcnow(),
                       "last_updated_by": "surya"}
     return facultydetails					  
	 
	 
def insert_bulk1(client,dict_list):
    db = client.collegedb
    db.facultydetails.insert_many(dict_list).inserted_ids
    print("successfully inserted data rows. count=%s" % db.facultydetails.count())
	
def payroll_json(row_index, col_range, sheet1, book):
     facul_id_value= int(sheet1.cell(row_index,0).value)
     facul_name_value= sheet1.cell(row_index,1).value
     reg_hours_value= int(sheet1.cell(row_index,19).value)
     vacation_value= int(sheet1.cell(row_index,20).value)
     sick_value= int(sheet1.cell(row_index,21).value)
     basicpay_value= int(sheet1.cell(row_index,22).value)
     deduction_value= int(sheet1.cell(row_index,23).value)
     grosspay_value= int(sheet1.cell(row_index,24).value)
     payroll_details={
	     "faculty_id":facul_id_value,
	     "faculty_name":facul_name_value,
	     "regular_working_hours":reg_hours_value,
	     "vacation_hours":vacation_value,
	     "sick_hours":sick_value,
         "basicpay":basicpay_value,
         "deduction":deduction_value,
         "grosspay": grosspay_value}
     return payroll_details

def insert_bulk2(client,dict_list):
    db = client.collegedb
    db.payroll_details.insert_many(dict_list).inserted_ids
    print("successfully inserted data rows. count=%s" % db.payroll_details.count())
		 
